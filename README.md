# Over mij

Mijn naam is Jildou Haaijer, ik ben een 3e jaars Bioinformatica student an de HanzeHogeschool.
Ik ben erg geïntereseerd in Machine Learning en Wiskunde, naast mijn studie vind ik het leuk om gitaar te spelen.

De een van de leukste projecten die ik tot nu toe heb gedaan is een website maken voor Dierenartspraktijk VanStadTotWad. Hierbij werd er onderzoek gedaan naar Salmonella infecties bij veebedrijven in Groningen. 
De salmonella infecties werden gevisualiseerd op een GIS kaart met een tijd lijn, hierdoor kon je de laterale verpreiding van de Salmonella langs de veebedrijven zien. Ik vond het heel leuk om uit te zoeken hoe de data het best gevisualiseerd kon worden en om nieuwe dingen aan de website toe te voegen.
Zoals een grafiekje bij de datapunten op de kaart, zodat je van een veebedrijf kon zien in welk jaren er wel een besmetting met Salmonella was en in welke jaren niet.
Hierbij heb ik geleerd om een Java-backend te maken en hoe je met GIS werkt.

Een ander project wat ik ook erg leuk vond was het classificeren van eiwitten op basis van hun locatie binnen een cel. Hierbij werd de locatie bepaald aan de hand van acht eigenschappen. Ik vond het erg leuk om de data te verkennen en op basis daarvan de 
beste eigenschappen te kiezen die het meest over de data zouden zeggen bij het classificeren met algoritmen in WEKA.

Een ander project wat ik ook erg leuk vond was het maken van een pipeline met snakemake, deze pipeline kon met Taiyaki een basecalling model hertrainen op specifieke data. Ik vond het vooral leuk 
om de pipeline zo efficient mogelijk te maken en alle stappen goed op elkaar aan te laten sluiten.
Ik zou mij graag verder specialiseren in Machine learning, met name in de biomedische richting. 
----

## Repositrories
- [Website van Stad tot Wad Repository.](https://bitbucket.org/jfhaaijer/salmonellosis/src/master/ "Website Repository")

- [Eiwit Localisatie Repository.](https://bitbucket.org/jfhaaijer/thema9_yeastproteinlocalisation/src/master/ "Eiwit Localisatie Repository")

- [Beevirus Repository.](https://bitbucket.org/jfhaaijer/beevirus1 "BeeVirus Repository")


